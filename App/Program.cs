﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Blackriverinc.Framework.DataStore;
using Xceed.Words.NET;

namespace App
   {
   public class Program
      {
      /// <summary>
      /// args[0]: Input stream specifier
      /// args[1..n]: Skills filters
      /// </summary>
      /// <param name="args"></param>
      public static void Main(string[] args)
         {
         Trace.Listeners.Add(new ConsoleTraceListener());

         try
            {
            if (args.Length < 1)
               throw new ArgumentOutOfRangeException($"'{nameof(args)}' is empty: include at least one filename on command line.");

            var input = StreamFactory.Create($"{args[0]}");
            using (var xdoc = DocX.Load(input))
               {
               Trace.WriteLine($"<!-- Document: {args[0]}");
               var visibleJobs = ParseDocument(xdoc, args.Skip(1).ToList());
               foreach (var job in visibleJobs)
                  {
                  Trace.WriteLine($"<!-- Paragraph {job.Item1.StyleName} -->");
                  Trace.WriteLine($"{job.Item1?.Text ?? ""}");
                  foreach(var paragraph in job.Item2)
                     {
                     Trace.WriteLine($"    {paragraph?.Text ?? ""}");
                     }
                  }
               }
            }
         catch (Exception exp)
            {
            Trace.WriteLine($"Error: {exp.Message}");
            }

         return;
         }

      public static List<Tuple<Paragraph, List<Paragraph>>> ParseDocument(DocX xdoc, List<string> filters)
         {
         List<Tuple<Paragraph, List<Paragraph>>> visibleJobs = 
                           new List<Tuple<Paragraph, List<Paragraph>>>();

         Paragraph jobParagraph = null;
         List<Paragraph> jobScope = null;

         Trace.WriteLine($"<!-- Filters: {string.Join(" ", filters)} -->");
         // ----------------------------------------------------------------------
         // DocX presents a Document as a collection of Paragraphs. Each Paragraph
         // has a defined Style. This system is meant to operate on a specifically
         // formatted Document, that has Jobs delimitted by Paragraph[StyleName="xjob"].
         // Within the scope of each Job We want to identify the Jobs that 
         // required use of a particular set of Skills.
         // ----------------------------------------------------------------------
         foreach (var paragraph in xdoc.Paragraphs)
            {
            if (paragraph.StyleName == "xjob")
               {
               // Mark the Job Header; starts a scope.
               jobParagraph = paragraph;
               jobScope = new List<Paragraph>();
               continue;
               }
             
            if (jobScope != null)
               jobScope.Add(paragraph);

            // -----------------------------------------------------------
            // Populate Skills in current Job.
            //
            // Skills are presented within the scope of a Job by either
            // a 'xskills' Paragraph, or it is a Normal Paragraph
            // containing a run of characters marked with 'xskillsChar'.
            // -----------------------------------------------------------
            IEnumerable<string> skills = new List<string>();
            Func<string, IEnumerable<string>> skillsParser =
               (text => from s in text.Split(new char[] { ',', ' ' },
                                                StringSplitOptions.RemoveEmptyEntries)
                        let skill = s.Replace("(", string.Empty)
                                     .Replace(")", string.Empty)
                        select skill);

            if (paragraph.StyleName == "xskills")
               skills = skills.Concat(skillsParser(paragraph.Text));
            else
               foreach (var mt in paragraph.MagicText
                                       .Where(mt => (mt.formatting?.StyleName ?? "")
                                                                  .StartsWith("xskills")))
                  {
                  skills = skills.Concat(skillsParser(mt.text));
                  }

            // -----------------------------------------------------------
            // Display Jobs matching Skills
            // -----------------------------------------------------------
            if (filters.Count == 0
            || filters.Intersect(skills, StringComparer.CurrentCultureIgnoreCase).Count() > 0)
               {
               visibleJobs.Add(Tuple.Create(jobParagraph, jobScope));
               }
            }

         return visibleJobs;

         }
      }
   }
